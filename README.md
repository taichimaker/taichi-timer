Arduino Taichi-Timer 库
========
by Taichi-Maker / www.taichi-maker.com

中文说明
--------
Arduino定时执行用户指定的任务。因为利用的是millis函数而没有使用中断所以不必担心硬件冲突。

English Description
--------------------
With this Arduino Lib, Arduino can execute user defined functions repeatedly. The interval can be set up according to user's needs. This lib applies Arduino's mills function. Hardware interrupt conflict is not a problem.



