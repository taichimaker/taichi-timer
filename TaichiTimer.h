#ifndef _TAICHI_TIMER_H_
#define _TAICHI_TIMER_H_

#include <Arduino.h>

class TaichiTimer{
  public:
    TaichiTimer(int usrInterval, void (*usrFuncPointer)());
    void runTimer();
    void setInterval(int usrInterval);
    int getInterval();

  private:
    int interval;
    unsigned long currentMillis;
    unsigned long previousMillis;
    bool ledState;
    void (*funcPointer)();
};


#endif
