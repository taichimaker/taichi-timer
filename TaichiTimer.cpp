#include "TaichiTimer.h"

TaichiTimer::TaichiTimer(int usrInterval, void (*usrFuncPointer)()){
  interval = usrInterval;
  funcPointer = usrFuncPointer;
}

void TaichiTimer::runTimer(){
  currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    (*funcPointer)();
  }
}

void TaichiTimer::setInterval(int usrInterval){
  interval = usrInterval;
}

int TaichiTimer::getInterval(){
  return interval;
}
