/*
  simple_Timer
  by Taichi-Maker
  www.taichi-maker.com
  2019-06-15

  本程序旨在使用Taichi-Timer库让Arduino定时执行用户函数。
  在此程序中，您将通过串口监视器看到testFunc1和testFunc2被反复调用。
  他们被调用的时间间隔是由TaichiTimer对象建立时的第一个参数所控制的。
  该参数单位为毫秒。
  This example code shows how to use Taichi-Timer lib to
  excute user functions.
*/

#include <TaichiTimer.h>

void testFunc1(); //定时执行的第一个函数testFunc1（注：必须在建立定时对象前声明函数，否则无法工作）
void testFunc2(); //定时执行的第二个函数testFunc2（注：必须在建立定时对象前声明函数，否则无法工作）
/*  testFunc1 and testFunc2 will be called at a fixed time interval.
 *  Attention: testfunc1 and testFunc2 must be declared before
 *  creating the TaichiTimer instance that calls them repeatedly. */

TaichiTimer myTimer1(1000, testFunc1);  //建立第一个定时对象，参数1000意味着该定时器将会每隔
                                        //1000毫秒调用一次函数testFunc1。
                                        //请注意：testFunc1必须在建立myTimer1前声明

TaichiTimer myTimer2(2000, testFunc2);  //建立第二个定时对象，参数2000意味着该定时器将会每隔
                                        //2000毫秒调用一次函数testFunc2。
                                        //请注意：testFunc2必须在建立myTimer2前声明
/* In the above code, testFunc1 and testFunc2 will be called at a fixed time interval.
 * The time intervals for testFunc1 and testFunc2 are 1000 ms and 2000 ms.
 * The intervals are set at the first parameter when creating the TaichiTimer instances. */

void setup() {
  Serial.begin(9600);
}

void loop() {
  myTimer1.runTimer();  // runTimer必须被经常调用，否则定时器将无法正常工作。
  myTimer2.runTimer();  // runTimer MUST be called constanstantly or the timers will NOT work.
}

//被反复调用的函数1
void testFunc1(){
  static int counter1;
  counter1++;
  Serial.print("testFunc1 called ");
  Serial.print(counter1);
  Serial.println(" times.);
  Serial.println("");
}

//被反复调用的函数2
void testFunc2(){
  Serial.println("testFunc2.");
}
